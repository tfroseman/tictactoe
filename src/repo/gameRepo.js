/**
 * This will map directly to the database and is not used directly by the app.
 * One layer up the service will make requests here so as to abstract database calls.
 * The app will see a database service and make calls to that. This will make testing easier
 * and in the event of a database change only the repo will need to be updated if
 * the interface changes.
 */
import {getManager} from "typeorm";

function create(body) {
}

function get(uuid){
    getManager().getRepository(userRepo).findOne({"uuid": uuid})
}
function update(body){

}

function remove(body){

}

export {create, get, update, remove}