/**
 * This will map directly to the database and is not used directly by the app.
 * One layer up the service will make requests here so as to abstract database calls.
 * The app will see a database service and make calls to that. This will make testing easier
 * and in the event of a database change only the user_repository will need to be updated if
 * the interface changes.
 */
import {getManager} from "typeorm"
import {User} from "../models/user"

const user_repository = () => {
    const userRepo = getManager().getRepository(User)
    return {
        async create(body) {
            return await userRepo.create(body)
        },

        async getAll() {
            return await userRepo.find()
        },

        async getByUUID(uuid) {
            return await userRepo.findOne({"uuid": uuid})
        },

        async update(body) {
            return await userRepo.update({"uuid": body.uuid, "user": body})
        },

        async remove(uuid) {
            return await userRepo.findOne({"uuid": uuid})
        }
    }
}

export {user_repository}