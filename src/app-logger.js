// Imports the Google Cloud client library for Bunyan
const bunyan = require( "bunyan" )
const { LoggingBunyan } = require( "@google-cloud/logging-bunyan" )
import { ErrorReporting } from "@google-cloud/error-reporting"

// Creates a Bunyan Stackdriver Logging client
const loggingBunyan = new LoggingBunyan( {
    projectId: "tictactoe-220102",
    keyFilename: "./tictactoe-8b2d022777e0.json"
} )

// Create a Bunyan logger that streams to Stackdriver Logging
// Logs will be written to: "projects/YOUR_PROJECT_ID/logs/bunyan_log"
const logger = bunyan.createLogger( {
    // The JSON payload of the log as it appears in Stackdriver Logging
    // will contain "name": "my-service"
    name: "tictactoe-server",
    streams: [
        // Log to the console at 'info' and above
        { stream: process.stdout, level: "info" },
        // And log to Stackdriver Logging, logging at 'info' and above
        //loggingBunyan.stream( "info" ),
    ],
} )

// Instantiates a client
const error_logger = new ErrorReporting( {
    projectId: "tictactoe-220102",
    keyFilename: "./tictactoe-74d6a1872716.json",
    ignoreEnvironmentCheck: false
} )

export { logger, error_logger }