import { Entity, Column, PrimaryGeneratedColumn, } from "typeorm"
import { user_repository } from "../repo/userRepo"

@Entity()
class User {

	@PrimaryGeneratedColumn( { type: "int" } )
	id = undefined

	@Column( "text" )
	nickname = ""

	@Column( "text" )
	email = ""

	@Column( "text" )
	password = ""

	@PrimaryGeneratedColumn( "uuid" )
	uui = ""

}

let user_model = ( userRepo = user_repository() ) => {
	return {
		create: ( user ) => {
			if ( user === ( null | undefined ) ) {
				return new Error( "User can not be undefined" )
			} else {
				//TODO validate user object
				return userRepo.create( user )

			}
		},

		get: ( uuid = null ) => {
			//console.log(typeof userRepo)
			if ( uuid === null ) {
				return userRepo.getAll()
			} else {
				let result = userRepo.getByUUID( uuid )
				if ( !result ) {
					return new Error( "User does not exist" )
				}
				return result
			}
		},

		update: ( user ) => {
			return userRepo.update( user )
		},

		delete: ( uuid ) => {
			if ( !userRepo.delete( uuid ) ) {
				return new Error( "User does not exist" )
			}
			return "User Deleted"
		},
	}
}

export { user_model, User }
