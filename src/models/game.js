import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToMany, JoinTable} from "typeorm";
import {User} from "./user";
import { repo } from "../repo/gameRepo"

@Entity()
class Game extends BaseEntity {

    @PrimaryGeneratedColumn({type: 'int'})
    id = undefined;

    @Column("simple-array")
    state = [];

    @ManyToMany(type => User, { cascade: false })
    @JoinTable()
    User_x = undefined;

    @ManyToMany(type => User, { cascade: false })
    @JoinTable()
    User_o = undefined;

    @ManyToMany(type => User, { cascade: false })
    @JoinTable()
    last_played = undefined;

    @Column("boolean")
    started = false;

    @ManyToMany(type => User, { cascade: false })
    @JoinTable()
    categories = undefined;

    @Column("timestamp")
    create_at = undefined;

    @Column("timestamp")
    updated_at = undefined;

}

let game = (gameRepo = repo()) => {
    return {
        /**
         * Creat a new game object
         * @param game
         * @returns {*}
         */
        create: (game) => {
            if(game === (null | undefined)) {
                return new Error("Error: Game is undefined")
            }
            //TODO validate game object
            return gameRepo.create(game)
        },

        /**
         * If uuid is set, get the requested game. If uuid is not passed, get all games
         * @param uuid Not required
         * @returns {*}
         */
        get: (uuid = null) => {
            if(uuid === null){
                return gameRepo.getAll()
            } else {

                let result = gameRepo.getByUUID(uuid)

                if(!result){
                    return new Error("Error: Game does not exist")
                }
                return result
            }
        },

        /**
         * Update a game object
         * @param game
         * @returns {*}
         */
        update: (game) => {
            return gameRepo.update(game)
        },

        /**
         * Delete the requested game
         * @param uuid
         * @returns {*}
         */
        delete: (uuid) => {
            if (!gameRepo.delete(uuid)) {
                return new Error("Error: Game does not exist")
            }
            //TODO figure out what to return on success
            return {}
        },
    }
};

export {game, Game}