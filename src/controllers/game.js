import {game} from "../models/game"
let { logger, error_logger } = require("../app-logger")

// Display list of all game.
exports.index = function(req, res) {
    game().getAll()
        .then(result => {
            res.send(result);
        })
        .catch( error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally(() => {
            logger.info(req)
        })
}

// Display detail page for a specific game.
exports.show = function(req, res) {
    game().getByUUID(req.param.uuid)
        .then(result => {
            res.send(result)
        })
        .catch( error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally( () => {
            logger.info(req)
        })
}

// Display game create form on GET.
exports.create = function(req, res) {
    res.status(404).send("Page not found")
}

// Handle game create on POST.
exports.store = function(req, res) {
    game().create(req.body)
        .then(result => {
            res.send(result)
        })
        .catch( error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally( () => {
            logger.info(req)
        })
}

// Display game delete form on GET.
exports.destroy = function(req, res) {
    game().delete(req.param.id)
        .then(result => {
            res.send(result)
        })
        .catch(error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally( () => {
            logger.info(req)
        })
}

// Display game update form on GET.
exports.edit = function(req, res) {
    res.status(404).send("Page not found")
}

// Handle game update on POST.
exports.update = function(req, res) {
    game().update(req.param.id, req.body)
        .then(result => {
            res.send(result)
        })
        .catch(error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally(() => {
            logger.info(req)
        })
};