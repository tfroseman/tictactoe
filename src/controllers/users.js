import {user_model} from "../models/user"
let { logger, error_logger } = require("../app-logger")

// Display list of all user.
exports.index = function (req, res) {
    user_model().get()
        .then( result => {
            res.send(result)
        })
        .catch( error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally(() => {
            //logger.info(req)
        })
}

// Display detail page for a specific user.
exports.show = function (req, res) {

    logger.info(req)

    user_model().get(req.params.uuid)
        .then(result => {
            res.send(result)
        })
        .catch( error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally(() => {
            logger.info(req)
        })
}

// Display user create form on GET.
// No need to send a page as this is just an api server
exports.create = function (req, res) {
    res.status(404).send("Page not found")
}

// Handle user create on POST.
exports.store = function (req, res) {
    user_model().create(req.data.body)
        .then( result => {
            res.send(result)
        })
        .catch( error => {
            error_logger.report(error)

            //TODO customize error message
            res.status(500).send(error)
        })
        .finally( () => {
            logger.info(req)
        })
}

// Display user delete form on GET.
exports.destroy = function (req, res) {
    user_model().delete(req.params.id)
        .then( result => {
            res.send(result)
        })
        .catch( error => {
            error_logger.report(error)

            //TODO customermize error message
            res.status(500).send(error)
        })
        .finally( () => {
            logger.info(req)
        })
}

// Display user update form on GET.
// No need to send a page as this is just an api server
exports.edit = function (req, res) {
    res.status(404).send("Page not found")
}

// Handle user update on POST.
exports.update = function (req, res) {
    user_model().update(req.body)
        .then(result => {
            res.send(result)
        })
        .catch(error => {
            error_logger.report(error)
        })
        .finally( () => {
            logger.info(req)
        })
}
