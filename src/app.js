//require("dotenv").config()
import "reflect-metadata"

let createError = require("http-errors")
let express = require("express")
let bodyParser = require("body-parser")
//let path = require("path")
let cookieParser = require("cookie-parser")
//let sassMiddleware = require("node-sass-middleware")
let { logger, error_logger } = require("./app-logger")

let app = express()
let indexRouter = require("./routes/index")
let usersRouter = require("./routes/users")
let gameRouter  = require("./routes/games")

// TODO remove as this is just an api server
// view engine setup
// app.set("views", path.join(__dirname, "views"))
// app.set("view engine", "pug")

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(cookieParser())

// TODO remove as this is just an api server
// app.use(sassMiddleware({
//     src: path.join(__dirname, "public"),
//     dest: path.join(__dirname, "public"),
//     indentedSyntax: true, // true = .sass and false = .scss
//     sourceMap: true
// }))
// app.use(express.static(path.join(__dirname, "public")))


// App Routes
app.use("/", indexRouter)
app.use("/users", usersRouter)
app.use("/games",gameRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404))
})

// error handler
// app.use(function(err, req, res, next) {
//     // set locals, only providing error in development
//     res.locals.message = err.message
//     //res.locals.error = req.app.get("env") === "development" ? err : {}
//     res.locals.error = err
//     error_logger.report(`${err}`)
//
//     // render the error page
//     res.status(err.status || 500)
//     res.render("error")
// })

app.use(error_logger.express)

module.exports = app
