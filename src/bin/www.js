#!/usr/bin/env node
import {} from "dotenv/config"

/**
 * Load database connection and model(entity) classes
 */
import {createConnection} from "typeorm"
import {Game} from "../models/game"
import {User} from "../models/user"

/**
 * Module dependencies.
 */

let app = require("../app")
let debug = require("debug")("tictactoe:server")
let http = require("http")

let database_config = {
    type: process.env.DB_TYPE,
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT,
    synchronize: process.env.DB_SYNCHRONIZE,
    logging: process.env.DB_LOGGING,
    entities: [User, Game],
}

createConnection(database_config)
    .then( async connection => {

        /**
       * Get port from environment and store in Express.
       */

        let port = normalizePort(process.env.SERVER_PORT || "3000")
        app.set("port", port)

        /**
       * Create HTTP server.
       */

        let server = http.createServer(app)

        /**
       * Listen on provided port, on all network interfaces.
       */

        server.listen(port)
        server.on("error", onError)
        server.on("listening", onListening)

        /**
       * Normalize a port into a number, string, or false.
       */

        function normalizePort(val) {
            let port = parseInt(val, 10)

            if (isNaN(port)) {
                // named pipe
                return val
            }

            if (port >= 0) {
                // port number
                return port
            }

            return false
        }

        /**
       * Event listener for HTTP server "error" event.
       */

        function onError(error) {
            if (error.syscall !== "listen") {
                throw error
            }

            let bind = typeof port === "string"
                ? "Pipe " + port
                : "Port " + port

            // handle specific listen errors with friendly messages
            switch (error.code) {
            case "EACCES":
                console.error(bind + " requires elevated privileges")
                process.exit(1)
                break
            case "EADDRINUSE":
                console.error(bind + " is already in use")
                process.exit(1)
                break
            default:
                throw error
            }
        }

        /**
       * Event listener for HTTP server "listening" event.
       */

        function onListening() {
            let addr = server.address()
            let bind = typeof addr === "string"
                ? "pipe " + addr
                : "port " + addr.port
            debug("Listening on " + bind)
        }


    }).catch(error => console.log("TypeORM connection error: ", error))

export {app}