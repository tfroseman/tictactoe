let express = require('express');
let router = express.Router();
let game = require('../controllers/game');

/**
 * Game model routes (Public expose)
 */
router.get('/', game.index);
router.get('/create', game.create);
router.post('/', game.create);
router.get('/:id', game.show);
router.get('/:id/edit', game.edit);
router.put('/:id', game.update);
router.delete('/:id', game.destroy);

/**
 *
 */

/**
 * Export game routes
 * @type {Router|router}
 */
module.exports = router;
