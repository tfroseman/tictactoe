let express = require("express")
let router = express.Router()
let users = require("../controllers/users")

router.get("/", users.index)
router.get("/create", users.create)
router.post("/", users.store)
router.get("/:uuid", users.show)
router.get("/:uuid/edit", users.edit)
router.put("/:uuid", users.update)
router.delete("/:uuid", users.destroy)

module.exports = router