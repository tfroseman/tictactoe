let express = require("express")
let index_router = express.Router()

/* GET home page. */
index_router.get("/", function(req, res, next) {
    res.render("index", { title: "Express" })
})

module.exports = index_router
