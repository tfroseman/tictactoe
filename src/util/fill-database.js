#!/usr/bin/env node

//TODO close connection after adding users.

let faker = require("faker")
const { Client } = require("pg")

const client = new Client({
    user: "",
    host: "localhost",
    database: "tictactoe",
    password: "",
    port: 5432,
})
client.connect()
    .then(() => {
        for (let i = 0; i < 10; i++) {
            // Insert 10 Users
            client.query("INSERT INTO \"public\".\"user\" VALUES(DEFAULT, $1::text, $2::text, $3::text, $4::uuid)",
                [faker.internet.userName(), faker.internet.email(), faker.internet.password(), faker.random.uuid()])
                .then( (result) => {
                    console.log(result)
                })
        }
    })
    .catch((error) => {
        console.error("Connection Error", error.stack)
    })
