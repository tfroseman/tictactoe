import {game} from "../../../models/game"

const repoMock = () => {
    return {
        create: (body) => {
            return gameRepo.create(body)
        },

        get: (uuid = null) => {
            return gameMap.get(uuid)
        },

        update: (body) => {
            return gameMap.update(body)
        },

        delete: (uuid) => {
            return gameMap.delete(uuid)
        }
    }
}

const gameMap = new Map()

let testGame1, testGame2, testGame3

let gameRepo = game(repoMock())

beforeAll(() => {
    /**
	 * Lets reset out fake database
	 */
    gameMap.clear()

    /**
	 * Reset our default users
	 * @type {{id: number, nickname: string, email: string, password: string, uui: string}}
	 */
    testGame1 =  {
        "id": 1,
        "nickname": "testUser1",
        "email": "testUser1@gmail.com",
        "password": "password",
        "uuid": "3kb4kfa9-4jd8-3js9-4928-3d7d4ddf4143"
    }

    testGame2 =  {
        "id": 2,
        "nickname": "testUser2",
        "email": "testUser2@gmail.com",
        "password": "password",
        "uuid": "1ebfdfb6-4b39-4e4e-8335-3d7d5ddf41e3"
    }

    testGame3 =  {
        "id": 3,
        "nickname": "testUser3",
        "email": "testUser3@gmail.com",
        "password": "password",
        "uuid": "3kbdofpl-90c7-3d3d-3243-3v7d59if4dy3"
    }

    /**
	 * Insert our cleaned users into the database where uuid will be the primary key
	 */
    gameMap.set(testGame1.uuid, testGame1)
    gameMap.set(testGame2.uuid, testGame2)
    gameMap.set(testGame3.uuid, testGame3)
})

describe("Get", () => {
    test("all games", () => {
        expect(gameRepo.get()).toBe(gameMap);
    })

    test("game by uuid", () => {
        expect(gameRepo.get(testGame1.uuid)).toBe(testGame1);
    })
})

describe("Delete", () => {
    test("game by uuid", () => {
        expect(gameRepo.delete(testGame1.uuid)).toBe({});
    })
})

describe("Update", () => {

})

describe("Create", () => {

})
