import {user} from "../../../models/user"

const repoMock = () => {
    return {
        /**
         * Add user to the database
         * @param body Inflated user object
         * @returns {Map<any, any>}
         */
        create: (body) => {
            return userMap.set(body.uuid, body)
        },

        /**
         * Get all users from the database
         * @returns {Map<any, any>}
         */
        getAll: () => {
            return userMap
        },

        /**
         * Get a single user but its uuid
         * @param uuid
         * @returns {any}
         */
        getByUUID: (uuid) => {
            return userMap.get(uuid)
        },

        /**
         * Update a user in the database
         * @param body
         * @returns {Map<any, any>}
         */
        update: (body) => {
            return userMap.set(body.uuid, body)
        },

        /**
         * Delete a user from the database
         * @param uuid
         * @returns {boolean}
         */
        delete: (uuid) => {
            return userMap.delete(uuid)
        }
    }
}

let userMap = new Map()

let testUser1, testUser2, testUser3, testUser4

let userRepo = user(repoMock())

beforeEach(() => {
    /**
     * Lets reset out fake database
     */
    userMap.clear()

    /**
     * Reset our default users
     * @type {{id: number, nickname: string, email: string, password: string, uui: string}}
     */
    testUser1 =  {
        "id": 1,
        "nickname": "testUser1",
        "email": "testUser1@gmail.com",
        "password": "password",
        "uuid": "37ac736f-a4f8-4250-9ec1-3b00312aa0fa"
    }

    testUser2 =  {
        "id": 2,
        "nickname": "testUser2",
        "email": "testUser2@gmail.com",
        "password": "password",
        "uuid": "01bd44ce-f462-4dcd-ab52-06ea9b14170e"
    }

    testUser3 =  {
        "id": 3,
        "nickname": "testUser3",
        "email": "testUser3@gmail.com",
        "password": "password",
        "uuid": "383bcbf4-ce0a-49eb-be82-2558ee512049"
    }

    testUser4 =  {
        "id": 4,
        "nickname": "testUser4",
        "email": "testUser4@gmail.com",
        "password": "password",
        "uuid": "3cafd679-da8d-482a-9f82-2e5821ee24d5"
    }

    /**
     * Insert our cleaned users into the database where uuid will be the primary key
     */
    userMap.set(testUser1.uuid, testUser1)
    userMap.set(testUser2.uuid, testUser2)
    userMap.set(testUser3.uuid, testUser3)
})

describe("Get", () => {
    test("all Users", () => {
        expect(userRepo.getAll()).toBe(userMap)
    })

    test("by uuid", () => {
        expect(userRepo.getByUUID(testUser2.uuid)).toBe(testUser2)
    })

    test("missing user", () => {
        expect(userRepo.getByUUID(testUser4.uuid)).toMatchObject({"Error": "User does not exist"})
    })

})

describe("Delete", () => {
    test("Delete user by uuid: Success", () => {
        // Make sure the user is in the map
        expect(userRepo.getByUUID(testUser2.uuid)).toBe(testUser2)

        // Remove the user
        userRepo.delete(testUser2.uuid)

        // Verify the user is gone
        expect(userRepo.getByUUID(testUser2.uuid)).toMatchObject({"Error": "User does not exist"})
    })

    test.skip("Delete user by uuid: missing uuid", () => {
        expect(userRepo.getByUUID(testUser1.uuid)).toBe(testUser1)

        userRepo.delete("")

        expect(userRepo.getByUUID(testUser1.uuid)).toBe(testUser1)
    })

    test.skip("Delete user by uuid: missing user", () => {
        // Test to delete user again via api
        expect(userRepo.delete(testUser4.uuid)).toMatchObject({"Error": "User does not exist"})
    })
})

describe("Update", () => {
    test("Success", () => {
        // Make sure user matches
        expect(userRepo.get(testUser2)).toBe(testUser2)

        // Update user
        testUser2.email = "someotheremail@gmail.com"
        userRepo.update(testUser2)

        expect(userRepo.getByUUID(testUser2.uuid)).toBe(testUser2)
    })

    test.skip("Failure: missing email", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.email = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toMatchObject({"ERROR": "Email can not be empty"})
    })

    test.skip("Failure: missing nickname", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.nickname = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toMatchObject({"ERROR": "Nickname can not be empty"})
    })

    test.skip("Failure: non-unique nickname", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.nickname = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toBe({"ERROR": "Nickname is already in use"})
    })

    test.skip("Failure: non-unique email", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.nickname = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toBe({"ERROR": "Email is already in use"})
    })

    test.skip("Failure: password is empty", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.nickname = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toBe({"ERROR": "Password can not be empty"})
    })
})

describe("Create", () => {
    test("Success", () => {
        //Make sure user does not exist
        expect(userRepo.getByUUID(testUser4.uuid)).not.toBe(testUser4)

        userRepo.create(testUser4)

        expect(userRepo.getByUUID(testUser4.uuid)).toBe(testUser4)
    })

    test.skip("Failure: missing email", () => {
        expect(false).toBe(true)
    })

    test.skip("Failure: missing nickname", () => {
        expect(false).toBe(true)
    })

    test.skip("Failure: non-unique nickname", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.nickname = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toBe({"ERROR": "Nickname is already in use"})
    })

    test.skip("Failure: non-unique email", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.nickname = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toBe({"ERROR": "Email is already in use"})
    })

    test.skip("Failure: password is empty", () => {
        expect(userRepo.get(testUser2)).toBe(testUser2)

        testUser2.nickname = ""

        expect(userRepo.update(testUser2)).not.toBe(testUser2)
        expect(userRepo.update(testUser2)).toBe({"ERROR": "Password can not be empty"})
    })
})